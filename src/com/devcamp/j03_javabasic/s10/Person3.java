package com.devcamp.j03_javabasic.s10;

public class Person3 {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person3(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person3() {
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + "]";
    }

}

