package com.devcamp.j03_javabasic.s10;

import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Order {
    int id;
    String customerName;
    long price;
    Date orderDate;
    boolean confirm;
    String[] items;

    // khởi tạo với tất cả tham số
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = true;
        this.items = new String[] { "Book", "Pen", "Ruler" };
    }

    // khởi tạo k tham số
    public Order() {
        this("2");
    }

    // khởi tạo với 1 tham số customerName
    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "Book", "Pen", "Ruler" };
    }

    public Order(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[] { "Book", "Pen", "Ruler" });
    }

    @Override
    public String toString() {
        // định dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi", "VN"));
        // định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        // return string
        return "Order [id=" + id
                + ", customerName=" + customerName
                + ", price=" + usNumberFormat.format(price)
                + ", orderDate=" + defaulTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                + ", confirm=" + confirm
                + ", items=" + Arrays.toString(items)
                + "]";
    }


}
