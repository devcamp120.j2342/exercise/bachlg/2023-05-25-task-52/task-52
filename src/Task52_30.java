import java.util.ArrayList;

public class Task52_30 {
    public static void main(String[] args) throws Exception{
        ArrayList<Task52_30_Person> arrList = new ArrayList<>();
        //khởi tạo person với các tham số khác nhau
        Task52_30_Person person0 = new Task52_30_Person();
        Task52_30_Person person1 = new Task52_30_Person("Devcamp");
        Task52_30_Person person2 = new Task52_30_Person("Bach", 28, 55.5);
        Task52_30_Person person3 = new Task52_30_Person("Le", 30, 68.8, 50000000, new String[] {"Little Camel"});
        //thêm obj person vào danh sách
        arrList.add(person0);
        arrList.add(person1);
        arrList.add(person2);
        arrList.add(person3);
        //in ra màn hình
        for(Task52_30_Person person : arrList){
            System.out.println(person.toString());
        } 
    }
}
