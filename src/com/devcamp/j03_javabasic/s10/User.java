package com.devcamp.j03_javabasic.s10;

public class User {
    String password;
    String username;
    boolean enabled;

    public User(String password, String username, boolean enabled) {
        this.password = password;
        this.username = username;
        this.enabled = enabled;
    }

    public User() {
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "User [password=" + password + ", username=" + username + ", enabled=" + enabled + "]";
    } 
}


