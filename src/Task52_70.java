import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j03_javabasic.s10.Order2;
import com.devcamp.j03_javabasic.s10.Person3;

public class Task52_70 {
    public static void main(String[] args) throws Exception {
        ArrayList<Order2> arrList = new ArrayList<Order2>();

        //Khởi tạo đối tượng Order2 với 4 loại constructor khác nhau
        Order2 order1 = new Order2();
        Order2 order2 = new Order2(2, "Anh", 200000, new Date(), true, new String[]{"Bút", "Sách"}, new Person3("Anh", 28));
        Order2 order3 = new Order2(3, "Hoàng", 300000);

        //Thêm các đối tượng vào ArrayList
        arrList.add(order1);
        arrList.add(order2);
        arrList.add(order3);

        //In kết quả ra console theo định dạng tiêu chuẩn Việt Nam
        for (Order2 order : arrList) {
            System.out.println(order.toString());
        }
    }
}

