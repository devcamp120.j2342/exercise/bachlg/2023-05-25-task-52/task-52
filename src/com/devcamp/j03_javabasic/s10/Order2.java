package com.devcamp.j03_javabasic.s10;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class Order2 {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    Boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    Person3 buyer;// người mua, là một object thuộc class Person

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    public String[] getItems() {
        return items;
    }

    public void setItems(String[] items) {
        this.items = items;
    }

    public Person3 getBuyer() {
        return buyer;
    }

    public void setBuyer(Person3 buyer) {
        this.buyer = buyer;
    }

    public Order2() {
        this("Nam");
    }

    public Order2(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "Book", "Pen", "Ruler" };
        this.buyer = new Person3("An", 30);
    }

    public Order2(int id, String customerName, long price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "Book", "Pen", "Ruler" };
        this.buyer = new Person3("An", 30);
    }

    public Order2(int id, String customerName, long price, Date orderDate, Boolean confirm, String[] items,
            Person3 buyer) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }

    @Override
    public String toString() {
        // định dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi", "VN"));
        // định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        return "Order2 [id=" + id 
        + ", customerName=" + customerName 
        + ", price=" + usNumberFormat.format(price) 
        + ", orderDate=" + defaulTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())  
        + ", confirm=" + confirm 
        + ", items=" + Arrays.toString(items) 
        + ", buyer=" + buyer + "]";
    }

}


