import java.util.ArrayList;

import com.devcamp.j03_javabasic.s10.User;

public class Task52_60 {
    public static void main(String[] args) throws Exception {
        ArrayList<User> arrList = new ArrayList<>();
        User User1 = new User();
        User User2 = new User("123456", "Bachlg", true);

        //thêm obj order vào list
        arrList.add(User1);
        arrList.add(User2);

        //in ra màn hình
        for(User user: arrList){
            System.out.println(user.toString());
        }
    }
}

