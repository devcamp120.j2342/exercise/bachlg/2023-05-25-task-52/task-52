import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j03_javabasic.s10.Order;

public class Task52_50 {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arrList = new ArrayList<>();
        //khởi tạo order với các tham số kahc1 nhau (khởi tạo 4 obj order)
        Order Order1 = new Order();
        Order Order2 = new Order("Lan");
        Order Order3 = new Order(3, "Long", 88000);
        Order Order4 = new Order(4, "Name", 75000, new Date(),false, new String[] {"hộp màu", "tẩy", "giấy màu"});
        //thêm obj order vào list
        arrList.add(Order1);
        arrList.add(Order2);
        arrList.add(Order3);
        arrList.add(Order4);

        //in ra màn hình
        for(Order order: arrList){
            System.out.println(order.toString());
        }
    }
}
