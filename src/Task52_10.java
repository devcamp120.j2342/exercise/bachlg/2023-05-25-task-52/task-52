import model.Person;

public class Task52_10 {
    public static void main(String[] args) throws Exception {
        Person person = new Person();
        Person person2 = new Person("Bach", "Le", "male", "0375511150", "giabach1532@gmail.com");
        System.out.println("Hello, person1: " + person.toString());
        System.out.println("Hello, person2: " + person2.toString());
        System.out.println(person.getFirstName());
        System.out.println(person2.getLastName());
    }
}

