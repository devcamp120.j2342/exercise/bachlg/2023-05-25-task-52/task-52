import java.text.DecimalFormat;
import java.util.Random;

public class Task52_80 {
    public static void main(String[] args) throws Exception {
        // task 1: Làm tròn n số sau dấu .
        System.out.println("\nTask 1:");
        System.out.println(task1(2.100212, 2));
        System.out.println(task1(2.100212, 3));
        System.out.println(task1(2100, 2));

        // task 2: Lấy 1 số random bất kỳ trong khoảng 2 số cho trước
        System.out.println("\nTask 2:");
        System.out.println(task2(1, 10));
        System.out.println(task2(10, 20));

        // task 3: Tính số pytago từ 2 số đã cho c2 = a2 + b2
        System.out.println("\nTask 3:");
        System.out.println(task3(2, 4));
        System.out.println(task3(3, 4));

        // task 4: Kiểm tra số đã cho có phải số chính phương hay không c = a2
        System.out.println("\nTask 4:");
        System.out.println(task4(64));
        System.out.println(task4(94));

        // task 5: Lấy số lớn hơn gần nhất của số đã cho chia hết cho 5
        System.out.println("\nTask 5:");
        System.out.println(task5(32));
        System.out.println(task5(137));

        // task 6: Kiểm tra đầu vào có phải số hay không
        System.out.println("\nTask 6:");
        Object[] inputs6 = { 12, "abcd", "12" };
        for (Object input : inputs6) {
            boolean isNumber = task6(input);
            System.out.println(isNumber);
        }

        // task 7: Kiểm tra số đã cho có phải lũy thừa của 2 hay không
        System.out.println("\nTask 7:");
        System.out.println(task7(16));
        System.out.println(task7(18));
        System.out.println(task7(256));

        // task 8: Kiểm tra số đã cho có phải số tự nhiên hay không
        System.out.println("\nTask 8:");
        System.out.println(task8(-15));
        System.out.println(task8(1));
        System.out.println(task8(1.2));

        // task 9: Thêm dấu , vào phần nghìn của mỗi số
        System.out.println("\nTask 9:");
        System.out.println(task9(1000));
        System.out.println(task9(10000.23));

        // task 10: Chuyển số từ hệ thập phân về hệ nhị phân
        System.out.println("\nTask 10:");
        System.out.println(task10(51));
        System.out.println(task10(4));

    }

    // task 1: Làm tròn n số sau dấu .
    public static String task1(double d, int n) {
        return String.format("%." + n + "f", d);
    }

    // task 2: Lấy 1 số random bất kỳ trong khoảng 2 số cho trước
    public static int task2(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    // task 3: Tính số pytago từ 2 số đã cho c2 = a2 + b2
    public static double task3(double a, double b) {
        return Math.sqrt(a * a + b * b);
    }

    // task 4: Kiểm tra số đã cho có phải số chính phương hay không c = a2
    public static boolean task4(int num) {
        int sqrt = (int) Math.sqrt(num);
        return (sqrt * sqrt == num);
    }

    // task 5: Lấy số lớn hơn gần nhất của số đã cho chia hết cho 5
    private static int task5(int num) {
        int nextMultipleOfFive = (num / 5) * 5 + 5;
        return nextMultipleOfFive;
    }

    // task 6: Kiểm tra đầu vào có phải số hay không
    private static boolean task6(Object obj) {
        if (obj instanceof Number) {
            return true;
        } else if (obj instanceof String) {
            try {
                Integer.parseInt((String) obj);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return false;
    }

    // task 7: Kiểm tra số đã cho có phải lũy thừa của 2 hay không
    public static boolean task7(int n) {
        if (n <= 0) {
            return false;
        }

        while (n % 2 == 0) {
            n /= 2;
        }

        return n == 1;
    }

    // task 8: Kiểm tra số đã cho có phải số tự nhiên hay không
    public static boolean task8(double n) {
        return n >= 1 && (int) n == n;
    }

    // task 9: Thêm dấu , vào phần nghìn của mỗi số
    public static String task9(double n) {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        return df.format(n);
    }

    // task 10: Chuyển số từ hệ thập phân về hệ nhị phân
    public static String task10(int n) {
        if (n == 0) {
            return "0";
        }

        StringBuilder sb = new StringBuilder();
        while (n > 0) {
            int remainder = n % 2;
            sb.append(remainder);
            n /= 2;
        }

        return sb.reverse().toString();
    }

}

