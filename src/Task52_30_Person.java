import java.util.Arrays;

public class Task52_30_Person {
    String name; // tên người
    int age; // tuổi
    double weight; // cân nặng
    long salary; // lương
    String[] pets; // thú nuôi

    // khởi tạo 1 tham số name
    public Task52_30_Person(String name) {
        this.name = name;
        this.age = 18;
        this.weight = 60.5;
        this.salary = 10000000;
        this.pets = new String[] { "Big Dog", "Small cat", "Gold Fish", "Red Parrot" };
    }

    // khởi tạo với tất cả tham số
    public Task52_30_Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }

    // khởi tạo không tham số
    public Task52_30_Person() {
        this("Bachlg");
    }

    // khởi tạo với 3 tham số
    public Task52_30_Person(String name, int age, double weight) {
        this(name, age, weight, 20000000, new String[] { "Big Dog", "Small cat", "Gold Fish", "Red Parrot" });
    }

    @Override
        public String toString() {
            return "Person [name=" + name + ", age=" + age + ", weight=" + weight + ", salary=" + salary 
            + ", pets=" + Arrays.toString(pets) + "]";
        }
}

